# my_maps


**But**

Vous aimeriez partir en vacances. Seulement voilà, vous pensez qu’il n’existe pas d’outil adapté pour trouver votre trajet.
Vous vous dites que finalement, les vacances attendront et vous avez bien raison!Vous décidez alors de concevoir un site qui permet de choisir sa route pour aller d’un point A à un point B.
Cependant, comme vous aimez faire du tourisme, vous décidez de faire apparaitre sur votre trajet, tous les monuments, musées, ou autres reliques qui en valent la peine. 
Ainsi, vous pourrez choisir non pas la route la plus rapide, ni la plus courte, mais celle qui vous fait profiter au mieux du paysage lors de votre voyage.


# Outil et technologies utilisé !

    -Utilisation de l'API Google Maps Javascript
    -Javascript
    
    
# Travail fait !

    -Mise en place de l'API Google
    -Tracer d'un itinéraire
    -Affichage de la fiche de route
    -Fulscreen de la map
    
    
# Chose à faire !

    -Lors d'un trajet pouvoir passer par tous les lieux touristique sur le chemin
    -Mettre en place les différents mode de transports(train,vélo,à pied...)
    -Personnaliser les icônes de trajet
    
